






//WKWKWKWKWKWKWKWKWKWK AKU GABISA SAMSEK SI MAS 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fuse.h>
#include <limits.h>
#include <time.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define BASE_DIR "/home/tzubast/modul_4/data/gallery"
#define LOG_FILE "logs-fuse.log"

void write_log(const char *status, const char *operation, const char *path) {
    FILE *log_file = fopen(LOG_FILE, "a");
    if (log_file == NULL) {
        perror("Error opening logs-fuse.log");
        exit(EXIT_FAILURE);
    }

    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char time_str[20];
    strftime(time_str, sizeof(time_str), "%d/%m/%Y-%H:%M:%S", tm_info);

    char log_line[512];
    snprintf(log_line, sizeof(log_line), "[%s]::%s::[%s]::%s\n", status, time_str, operation, path);
    fputs(log_line, log_file);
    fclose(log_file);
}

static int hello_opendir(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, path);

    DIR *dir = opendir(full_path);
    if (dir == NULL) {
        res = -errno;
    } else {
        struct dirent *dp;
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                res = ((fuse_fill_dir_t)fi->fh)(fi->fh, dp->d_name, NULL, 0);
                if (res != 0) {
                    break;
                }
            }
        }
        closedir(dir);
    }

    return res;
}

static void create_test_reverse_file() {
    char sisop_path[PATH_MAX];
    char test_path[PATH_MAX];

    sprintf(sisop_path, "%s/sisop", BASE_DIR);
    sprintf(test_path, "%s/testfile_reverse", BASE_DIR);

    DIR *dir = opendir(sisop_path);
    if (dir == NULL) {
        perror("Error opening sisop directory");
        exit(EXIT_FAILURE);
    }

    FILE *test_file = fopen(test_path, "w");
    if (test_file == NULL) {
        perror("Error creating testfile_reverse");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", sisop_path, entry->d_name);

            FILE *sisop_file = fopen(file_path, "r");
            if (sisop_file == NULL) {
                perror("Error opening sisop file");
                exit(EXIT_FAILURE);
            }

            fseek(sisop_file, 0, SEEK_END);
            long file_size = ftell(sisop_file);
            fseek(sisop_file, 0, SEEK_SET);

            char *content = (char *)malloc(file_size + 1);
            fread(content, 1, file_size, sisop_file);
            content[file_size] = '\0';

            fclose(sisop_file);

            for (int i = 0, j = file_size - 1; i < j; ++i, --j) {
                char temp = content[i];
                content[i] = content[j];
                content[j] = temp;
            }

            fprintf(test_file, "%s\n", content);

            free(content);
        }
    }

    fclose(test_file);
    closedir(dir);

    write_log("SUCCESS", "create_test_reverse_file", test_path);
}

void reverse_filename(char *filename) {
    int i, j;
    char temp;
    int length = strlen(filename);

    for (i = 0, j = length - 1; i < j; ++i, --j) {
        temp = filename[i];
        filename[i] = filename[j];
        filename[j] = temp;
    }
}

static void remove_directory_contents(const char *base_path, const char *dir_name) {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "%s/%s", base_path, dir_name);

    DIR *dir = opendir(dir_path);
    if (dir == NULL) {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", dir_path, entry->d_name);
            remove(file_path);
        }
    }

    closedir(dir);
}

static void create_special_directories() {
    char rev_path[PATH_MAX];
    char delete_path[PATH_MAX];

    sprintf(rev_path, "%s/rev", BASE_DIR);
    sprintf(delete_path, "%s/delete", BASE_DIR);

    if (mkdir(rev_path, 0755) == -1) {
        perror("Error creating rev directory");
        exit(EXIT_FAILURE);
    }

    if (mkdir(delete_path, 0755) == -1) {
        perror("Error creating delete directory");
        exit(EXIT_FAILURE);
    }

    write_log("SUCCESS", "mkdir", rev_path);
    write_log("SUCCESS", "mkdir", delete_path);
}

static void modify_script_permissions() {
    char script_path[PATH_MAX];
    sprintf(script_path, "%s/gallery/script.sh", BASE_DIR);

    if (chmod(script_path, S_IRWXU) == -1) {
        perror("Error changing permissions for script.sh");
        exit(EXIT_FAILURE);
    }

    remove_directory_contents(BASE_DIR, "gallery");
    remove_directory_contents(BASE_DIR, "sisop");
    remove_directory_contents(BASE_DIR, "tulisan");

    write_log("SUCCESS", "modify_script_permissions", script_path);
}

static void create_test_file() {
    char sisop_path[PATH_MAX];
    char test_path[PATH_MAX];

    sprintf(sisop_path, "%s/sisop", BASE_DIR);
    sprintf(test_path, "%s/testfile", BASE_DIR);

    DIR *dir = opendir(sisop_path);
    if (dir == NULL) {
        perror("Error opening sisop directory");
        exit(EXIT_FAILURE);
    }

    FILE *test_file = fopen(test_path, "w");
    if (test_file == NULL) {
        perror("Error creating testfile");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = opendir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", sisop_path, entry->d_name);

            FILE *sisop_file = fopen(file_path, "r");
            if (sisop_file == NULL) {
                perror("Error opening sisop file");
                exit(EXIT_FAILURE);
            }

            fseek(sisop_file, 0, SEEK_END);
            long file_size = ftell(sisop_file);
            fseek(sisop_file, 0, SEEK_SET);

            char *content = (char *)malloc(file_size + 1);
            fread(content, 1, file_size, sisop_file);
            content[file_size] = '\0';

            fclose(sisop_file);

            for (int i = 0, j = file_size - 1; i < j; ++i, --j) {
                char temp = content[i];
                content[i] = content[j];
                content[j] = temp;
            }

            fprintf(test_file, "%s\n", content);

            free(content);
        }
    }

    fclose(test_file);
    closedir(dir);

    write_log("SUCCESS", "create_test_file", test_path);
}


static void base64_decode(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_read(bio, output, strlen(input));
    
    output[strlen(input)] = '\0';

    BIO_free_all(bio);
}

static void decode_file(const char *input_path, char *output_path, const char *prefix) {
    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, input_path);

    FILE *input_file = fopen(full_path, "r");
    if (input_file == NULL) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    char *content = (char *)malloc(file_size + 1);
    fread(content, 1, file_size, input_file);
    content[file_size] = '\0';

    fclose(input_file);

    if (strcmp(prefix, "base64") == 0) {
        // Decode dengan algoritma Base64
        char decoded_content[file_size];
        base64_decode(content, decoded_content);

        snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, output_path);
        FILE *output_file = fopen(full_path, "w");
        if (output_file == NULL) {
            perror("Error creating output file");
            exit(EXIT_FAILURE);
        }

        fprintf(output_file, "%s", decoded_content);

        fclose(output_file);
    } else {
    }

    free(content);
}

static int hello_getattr(const char *path, struct stat *stbuf) {
    int res = 0;
    char full_path[PATH_MAX];

    snprintf(full_path, PATH_MAX, "%s%s", BASE_DIR, path);

    memset(stbuf, 0, sizeof(struct stat));

    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        if (access(full_path, F_OK) == 0) {
            struct stat file_stat;
            if (stat(full_path, &file_stat) == 0) {
                stbuf->st_mode = file_stat.st_mode;
                stbuf->st_nlink = file_stat.st_nlink;
                stbuf->st_size = file_stat.st_size;
                stbuf->st_atime = file_stat.st_atime;
                stbuf->st_mtime = file_stat.st_mtime;
                stbuf->st_ctime = file_stat.st_ctime;
            } else {
                perror("Error getting file attributes");
                res = -1;
            }
        } else {
            res = -ENOENT;
        }
    }

    if (res == 0) {
        write_log("SUCCESS", "getattr", path);
    } else {
        write_log("FAILED", "getattr", path);
    }

    return res;
}

static int hello_rename(const char *oldpath, const char *newpath) {
    int res = 0;

    char old_full_path[PATH_MAX];
    char new_full_path[PATH_MAX];
    sprintf(old_full_path, "%s%s", BASE_DIR, oldpath);
    sprintf(new_full_path, "%s%s", BASE_DIR, newpath);

    if (strcmp(old_full_path, new_full_path) == 0) {
        write_log("SUCCESS", "rename", oldpath);
        return res;
    }

    struct stat old_stat, new_stat;
    if (stat(old_full_path, &old_stat) == 0 && stat(new_full_path, &new_stat) == 0 &&
        S_ISREG(old_stat.st_mode) && S_ISREG(new_stat.st_mode)) {
        if (rename(old_full_path, new_full_path) == -1) {
            perror("Error renaming file");
            res = -1;
        }
    } else {
        if (mkdir(new_full_path, 0755) == -1) {
            perror("Error creating destination directory");
            res = -1;
        } else {
            if (rename(old_full_path, new_full_path) == -1) {
                perror("Error moving directory");
                res = -1;
            }
        }
    }

    if (res == 0) {
        write_log("SUCCESS", "rename", oldpath);
    } else {
        write_log("FAILED", "rename", oldpath);
    }

    return res;
}

int hello_unlink(const char *path) {    int res = 0;

    char full_path[PATH_MAX];
    sprintf(full_path, "%s%s", BASE_DIR, path);

    if (remove(full_path) == -1) {
        perror("Error removing file");
        res = -1;
    }

    if (res == 0) {
        write_log("SUCCESS", "unlink", path);
    } else {
        write_log("FAILED", "unlink", path);
    }

    return res;
}

static int hello_opendir(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, path);

    DIR *dir = opendir(full_path);
    if (dir == NULL) {
        res = -errno;
    } else {
        struct dirent *dp;
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                if (fuse_fill_dir(fi->fh, dp->d_name, NULL, 0) != 0) {
                    res = -errno;
                    break;
                }
            }
        }
        closedir(dir);
    }

    return res;
}

static int hello_open(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    sprintf(full_path, "%s%s", BASE_DIR, path);

    int fd = open(full_path, fi->flags);
    if (fd == -1) {
        perror("Error opening file");
        res = -1;
    } else {
        fi->fh = fd;
    }

    if (res == 0) {
        write_log("SUCCESS", "open", path);
    } else {
        write_log("FAILED", "open", path);
        close(fd);
    }

    return res;
}

static int hello_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int res = pread(fi->fh, buf, size, offset);
    if (res == -1) {
        perror("Error reading file");
    } else {
        write_log("SUCCESS", "read", path);
    }

    return res;
}

int main(int argc, char *argv[]) {
    FILE *log_file = fopen(LOG_FILE, "w");
    if (log_file != NULL) {
        fclose(log_file);
    } else {
        perror("Error creating logs-fuse.log");
        exit(EXIT_FAILURE);
    }

    create_special_directories();
    modify_script_permissions();
    create_test_file();

   struct fuse_operations_compat2 hello_oper = {
    .getattr = hello_getattr,
    .opendir = hello_opendir,  
    .open = hello_open,
    .read = hello_read,
    .rename = hello_rename,
    .unlink = hello_unlink,
};

return fuse_main(argc, argv, &hello_oper, NULL);
}