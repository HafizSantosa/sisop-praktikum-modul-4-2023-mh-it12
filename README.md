# sisop-praktikum-modul-4-2023-MH-IT12

Laporan Resmi Praktikum Sistem Operasi Modul 4
Kelompok IT12:

+ Muhammad Afif (5027221032)
+ Hafiz Akmaldi Santosa (5027221061)
+ Nur Azka Rahadiansyah (5027221064)


Pengantar

Laporan resmi ini dibuat terkait dengan praktikum modul 4 sistem operasi yang telah dilaksanakan pada tanggal 06 November 2023 hingga tanggal 12 November 2023. Praktikum modul 4 terdiri dari 3 soal dengan tema C yang dikerjakan oleh kelompok praktikan yang terdiri dari 3 orang selama waktu tertentu.
Kelompok IT12 melakukan pengerjaan modul 3 ini dengan pembagian sebagai berikut:


- Soal 1 dikerjakan oleh Nur Azka
- Soal 2 dikerjakan oleh Hafiz A.
- Soal 3 dikerjakan oleh M. Afif

Sehingga dengan demikian, semua anggota kelompok memiliki peran yang sama besar dalam pengerjaan modul 4 ini.

Kelompok IT12 juga telah menyelesaikan tugas praktikum modul 4 yang telah diberikan dan telah melakukan demonstrasi kepada Asisten lab penguji Mbak Yuniar. Dari hasil praktikum yang telah dilakukan sebelumnya, maka diperoleh hasil sebagaimana yang dituliskan pada setiap bab di bawah ini.

## Soal 1
Dalam soal ini kita diminta untuk membuat program *hell.c* yang dapat melakukan beberapa ketentuan yang di berikan pada soal.

### Isi soal
#### Implementasi Fungsi untuk Folder "gallery":

- Membuat fungsi untuk folder "gallery":
Membuat folder dengan prefix "rev."
- Implementasi logika untuk pembalikan nama file saat dipindahkan ke dalam folder "rev."

#### Implementasi Fungsi untuk Folder "delete" di "gallery":

Membuat fungsi untuk folder "delete" di "gallery":
- Implementasi logika penghapusan file saat dipindahkan ke dalam folder "delete."

#### Penyesuaian Permissions pada File "script.sh" di Folder "sisop":

Membuat fungsi untuk menyesuaikan permissions pada file "script.sh" di folder "sisop":

- Mengubah permissions agar file tidak dapat menghapus isi dari beberapa folder.

#### Fungsi Pembuatan File Baru dengan Prefix "test" di Folder "sisop":

Membuat fungsi pembuatan file baru dengan prefix "test" di folder "sisop":

- Implementasi logika untuk pembalikan (reverse) isi dari file yang dibuat.

#### Penanganan File Teks dengan Prefix Khusus di Folder "tulisan":

Membuat fungsi Fuse untuk mengelola berkas teks dengan penanganan prefix khusus:

- Dekode otomatis tergantung pada prefix yang terdapat dalam nama berkas (base64, rot13, hex, rev).

#### Implementasi Kebijakan Keamanan pada Folder "disable-area":

- Mengunci file yang ada di folder "disable-area" dengan password.

#### Pencatatan Setiap Proses pada logs-fuse.log:

- Membuat mekanisme pencatatan setiap proses yang dilakukan pada logs-fuse.log dengan format:
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]

## Penyelesaian
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fuse.h>
#include <limits.h>
#include <time.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define BASE_DIR "/home/tzubast/modul_4/data/gallery"
#define LOG_FILE "logs-fuse.log"

void write_log(const char *status, const char *operation, const char *path) {
    FILE *log_file = fopen(LOG_FILE, "a");
    if (log_file == NULL) {
        perror("Error opening logs-fuse.log");
        exit(EXIT_FAILURE);
    }

    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char time_str[20];
    strftime(time_str, sizeof(time_str), "%d/%m/%Y-%H:%M:%S", tm_info);

    char log_line[512];
    snprintf(log_line, sizeof(log_line), "[%s]::%s::[%s]::%s\n", status, time_str, operation, path);
    fputs(log_line, log_file);
    fclose(log_file);
}

static int hello_opendir(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, path);

    DIR *dir = opendir(full_path);
    if (dir == NULL) {
        res = -errno;
    } else {
        struct dirent *dp;
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                res = ((fuse_fill_dir_t)fi->fh)(fi->fh, dp->d_name, NULL, 0);
                if (res != 0) {
                    break;
                }
            }
        }
        closedir(dir);
    }

    return res;
}

static void create_test_reverse_file() {
    char sisop_path[PATH_MAX];
    char test_path[PATH_MAX];

    sprintf(sisop_path, "%s/sisop", BASE_DIR);
    sprintf(test_path, "%s/testfile_reverse", BASE_DIR);

    DIR *dir = opendir(sisop_path);
    if (dir == NULL) {
        perror("Error opening sisop directory");
        exit(EXIT_FAILURE);
    }

    FILE *test_file = fopen(test_path, "w");
    if (test_file == NULL) {
        perror("Error creating testfile_reverse");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", sisop_path, entry->d_name);

            FILE *sisop_file = fopen(file_path, "r");
            if (sisop_file == NULL) {
                perror("Error opening sisop file");
                exit(EXIT_FAILURE);
            }

            fseek(sisop_file, 0, SEEK_END);
            long file_size = ftell(sisop_file);
            fseek(sisop_file, 0, SEEK_SET);

            char *content = (char *)malloc(file_size + 1);
            fread(content, 1, file_size, sisop_file);
            content[file_size] = '\0';

            fclose(sisop_file);

            for (int i = 0, j = file_size - 1; i < j; ++i, --j) {
                char temp = content[i];
                content[i] = content[j];
                content[j] = temp;
            }

            fprintf(test_file, "%s\n", content);

            free(content);
        }
    }

    fclose(test_file);
    closedir(dir);

    write_log("SUCCESS", "create_test_reverse_file", test_path);
}

void reverse_filename(char *filename) {
    int i, j;
    char temp;
    int length = strlen(filename);

    for (i = 0, j = length - 1; i < j; ++i, --j) {
        temp = filename[i];
        filename[i] = filename[j];
        filename[j] = temp;
    }
}

static void remove_directory_contents(const char *base_path, const char *dir_name) {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "%s/%s", base_path, dir_name);

    DIR *dir = opendir(dir_path);
    if (dir == NULL) {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", dir_path, entry->d_name);
            remove(file_path);
        }
    }

    closedir(dir);
}

static void create_special_directories() {
    char rev_path[PATH_MAX];
    char delete_path[PATH_MAX];

    sprintf(rev_path, "%s/rev", BASE_DIR);
    sprintf(delete_path, "%s/delete", BASE_DIR);

    if (mkdir(rev_path, 0755) == -1) {
        perror("Error creating rev directory");
        exit(EXIT_FAILURE);
    }

    if (mkdir(delete_path, 0755) == -1) {
        perror("Error creating delete directory");
        exit(EXIT_FAILURE);
    }

    write_log("SUCCESS", "mkdir", rev_path);
    write_log("SUCCESS", "mkdir", delete_path);
}

static void modify_script_permissions() {
    char script_path[PATH_MAX];
    sprintf(script_path, "%s/gallery/script.sh", BASE_DIR);

    if (chmod(script_path, S_IRWXU) == -1) {
        perror("Error changing permissions for script.sh");
        exit(EXIT_FAILURE);
    }

    remove_directory_contents(BASE_DIR, "gallery");
    remove_directory_contents(BASE_DIR, "sisop");
    remove_directory_contents(BASE_DIR, "tulisan");

    write_log("SUCCESS", "modify_script_permissions", script_path);
}

static void create_test_file() {
    char sisop_path[PATH_MAX];
    char test_path[PATH_MAX];

    sprintf(sisop_path, "%s/sisop", BASE_DIR);
    sprintf(test_path, "%s/testfile", BASE_DIR);

    DIR *dir = opendir(sisop_path);
    if (dir == NULL) {
        perror("Error opening sisop directory");
        exit(EXIT_FAILURE);
    }

    FILE *test_file = fopen(test_path, "w");
    if (test_file == NULL) {
        perror("Error creating testfile");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = opendir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char file_path[PATH_MAX];
            sprintf(file_path, "%s/%s", sisop_path, entry->d_name);

            FILE *sisop_file = fopen(file_path, "r");
            if (sisop_file == NULL) {
                perror("Error opening sisop file");
                exit(EXIT_FAILURE);
            }

            fseek(sisop_file, 0, SEEK_END);
            long file_size = ftell(sisop_file);
            fseek(sisop_file, 0, SEEK_SET);

            char *content = (char *)malloc(file_size + 1);
            fread(content, 1, file_size, sisop_file);
            content[file_size] = '\0';

            fclose(sisop_file);

            for (int i = 0, j = file_size - 1; i < j; ++i, --j) {
                char temp = content[i];
                content[i] = content[j];
                content[j] = temp;
            }

            fprintf(test_file, "%s\n", content);

            free(content);
        }
    }

    fclose(test_file);
    closedir(dir);

    write_log("SUCCESS", "create_test_file", test_path);
}


static void base64_decode(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_read(bio, output, strlen(input));
    
    output[strlen(input)] = '\0';

    BIO_free_all(bio);
}

static void decode_file(const char *input_path, char *output_path, const char *prefix) {
    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, input_path);

    FILE *input_file = fopen(full_path, "r");
    if (input_file == NULL) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    char *content = (char *)malloc(file_size + 1);
    fread(content, 1, file_size, input_file);
    content[file_size] = '\0';

    fclose(input_file);

    if (strcmp(prefix, "base64") == 0) {
        // Decode dengan algoritma Base64
        char decoded_content[file_size];
        base64_decode(content, decoded_content);

        snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, output_path);
        FILE *output_file = fopen(full_path, "w");
        if (output_file == NULL) {
            perror("Error creating output file");
            exit(EXIT_FAILURE);
        }

        fprintf(output_file, "%s", decoded_content);

        fclose(output_file);
    } else {
    }

    free(content);
}

static int hello_getattr(const char *path, struct stat *stbuf) {
    int res = 0;
    char full_path[PATH_MAX];

    snprintf(full_path, PATH_MAX, "%s%s", BASE_DIR, path);

    memset(stbuf, 0, sizeof(struct stat));

    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        if (access(full_path, F_OK) == 0) {
            struct stat file_stat;
            if (stat(full_path, &file_stat) == 0) {
                stbuf->st_mode = file_stat.st_mode;
                stbuf->st_nlink = file_stat.st_nlink;
                stbuf->st_size = file_stat.st_size;
                stbuf->st_atime = file_stat.st_atime;
                stbuf->st_mtime = file_stat.st_mtime;
                stbuf->st_ctime = file_stat.st_ctime;
            } else {
                perror("Error getting file attributes");
                res = -1;
            }
        } else {
            res = -ENOENT;
        }
    }

    if (res == 0) {
        write_log("SUCCESS", "getattr", path);
    } else {
        write_log("FAILED", "getattr", path);
    }

    return res;
}

static int hello_rename(const char *oldpath, const char *newpath) {
    int res = 0;

    char old_full_path[PATH_MAX];
    char new_full_path[PATH_MAX];
    sprintf(old_full_path, "%s%s", BASE_DIR, oldpath);
    sprintf(new_full_path, "%s%s", BASE_DIR, newpath);

    if (strcmp(old_full_path, new_full_path) == 0) {
        write_log("SUCCESS", "rename", oldpath);
        return res;
    }

    struct stat old_stat, new_stat;
    if (stat(old_full_path, &old_stat) == 0 && stat(new_full_path, &new_stat) == 0 &&
        S_ISREG(old_stat.st_mode) && S_ISREG(new_stat.st_mode)) {
        if (rename(old_full_path, new_full_path) == -1) {
            perror("Error renaming file");
            res = -1;
        }
    } else {
        if (mkdir(new_full_path, 0755) == -1) {
            perror("Error creating destination directory");
            res = -1;
        } else {
            if (rename(old_full_path, new_full_path) == -1) {
                perror("Error moving directory");
                res = -1;
            }
        }
    }

    if (res == 0) {
        write_log("SUCCESS", "rename", oldpath);
    } else {
        write_log("FAILED", "rename", oldpath);
    }

    return res;
}

int hello_unlink(const char *path) {    int res = 0;

    char full_path[PATH_MAX];
    sprintf(full_path, "%s%s", BASE_DIR, path);

    if (remove(full_path) == -1) {
        perror("Error removing file");
        res = -1;
    }

    if (res == 0) {
        write_log("SUCCESS", "unlink", path);
    } else {
        write_log("FAILED", "unlink", path);
    }

    return res;
}

static int hello_opendir(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    snprintf(full_path, sizeof(full_path), "%s%s", BASE_DIR, path);

    DIR *dir = opendir(full_path);
    if (dir == NULL) {
        res = -errno;
    } else {
        struct dirent *dp;
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                if (fuse_fill_dir(fi->fh, dp->d_name, NULL, 0) != 0) {
                    res = -errno;
                    break;
                }
            }
        }
        closedir(dir);
    }

    return res;
}

static int hello_open(const char *path, struct fuse_file_info *fi) {
    int res = 0;

    char full_path[PATH_MAX];
    sprintf(full_path, "%s%s", BASE_DIR, path);

    int fd = open(full_path, fi->flags);
    if (fd == -1) {
        perror("Error opening file");
        res = -1;
    } else {
        fi->fh = fd;
    }

    if (res == 0) {
        write_log("SUCCESS", "open", path);
    } else {
        write_log("FAILED", "open", path);
        close(fd);
    }

    return res;
}

static int hello_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int res = pread(fi->fh, buf, size, offset);
    if (res == -1) {
        perror("Error reading file");
    } else {
        write_log("SUCCESS", "read", path);
    }

    return res;
}

int main(int argc, char *argv[]) {
    FILE *log_file = fopen(LOG_FILE, "w");
    if (log_file != NULL) {
        fclose(log_file);
    } else {
        perror("Error creating logs-fuse.log");
        exit(EXIT_FAILURE);
    }

    create_special_directories();
    modify_script_permissions();
    create_test_file();

   struct fuse_operations_compat2 hello_oper = {
    .getattr = hello_getattr,
    .opendir = hello_opendir,  
    .open = hello_open,
    .read = hello_read,
    .rename = hello_rename,
    .unlink = hello_unlink,
};

return fuse_main(argc, argv, &hello_oper, NULL);
}
```


## Soal 2

## Soal 3
Soal ini bermaksud untuk membuat sebuah file c yang dapat melakukan mount filesystem sedemikian sehingga setiap aktivitas pada filesystem dapat terekam pada sebuah log file, soal juga bermaksud agar file yang masuk ke dalam direktori modular akan dimodularisasi.

## Isi soal
Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:
+ Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular.
+ Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.
+ Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
    - Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
    - Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
    - Format untuk logging yaitu sebagai berikut.
        ```
        [LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]
        ```
        Contoh:
        REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
        REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
        FLAG::231105-12:29:33::RMDIR::/home/sarah/folder
        
    - Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
    Contoh:
    file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
    
    - Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

## Penyelesaian
`easy.c`
```c
#define FUSE_USE_VERSION 28
#define _XOPEN_SOURCE 500
#define HAVE_SETXATTR
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include<sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/xattr.h>

static const char *dirPath = "/home/tezla/moodular";
static const char *logfile = "/home/tezla/fs_module.log";

void ez_logg(const char *path, const char *aksi, const char *logtype) {
    char laporan[1000];
    char currtime[20];
    time_t wakt;
    struct tm *thisWaktu;

    time(&wakt);
    thisWaktu = localtime(&wakt);
    strftime(currtime, sizeof(currtime), "%y%m%d-%H:%M:%S", thisWaktu);
    sprintf(laporan, "%s::%s::%s::%s\n", logtype, currtime, aksi, path);

    FILE *inilog = fopen(logfile, "a");
    fprintf(inilog, "%s", laporan);
    printf("> Logged\n");

    fclose(inilog);
}

struct file {
    char filename[64];
};


//only file
void fileNot_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char mergeFile[320];
        char rmFile[256];
        sprintf(mergeFile, "cat %s.* > %s", filePath, filePath);
        sprintf(rmFile, "rm %s.*", filePath);

        system(mergeFile);
        system(rmFile);
    } else return;
}


//only file
void fileTo_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char splitFile[320];
        char rmFile[256];
        sprintf(splitFile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filePath, filePath);
        sprintf(rmFile, "rm %s", filePath);

        system(splitFile);
        system(rmFile);
    } else return;
}


//change back recursive
void changeTo_not_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4 && (strcmp(de->d_name, ".")!=0 && strcmp(de->d_name, "..")!=0)) {
            char newPath[320];
            sprintf(newPath, "%s/%s", rilPath, de->d_name);
            changeTo_not_modularDir(newPath);
        }
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//change recursive
void changeTo_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) {
            if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
            else {
                char newPath[320];
                sprintf(newPath, "%s/%s", rilPath, de->d_name);
                changeTo_modularDir(newPath);
            }
        }
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}


//for releasedir
void dont_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//for readdir
void do_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) continue;
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}

static int ez_getattr(const char *path, struct stat *statbuf) {
    int hasil;
    char rilPath[256];
    sprintf(rilPath, "%s%s", dirPath, path);

    hasil = lstat(rilPath, statbuf);
    if(hasil == -1) return -errno;

    return 0;
}

static int ez_truncate(const char *path, off_t size) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil;
    hasil = truncate(rilPath, size);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil;
    hasil = ftruncate(fi->fh, size);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_opendir(const char *path, struct fuse_file_info *fi) {
    (void)fi;
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) dont_moduleDir(rilPath);
    return 0;
}

static int ez_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *ingfoFile) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    DIR *iniDir;
    struct dirent *de;
    (void) offset;
    (void) ingfoFile;

    int hasil = 0;

    iniDir = opendir(rilPath);
    if(iniDir == NULL) return -errno;

    while((de = readdir(iniDir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type <<12;

        hasil = (filler(buf, de->d_name, &st, 0));

        if(hasil != 0) break;
    }

    closedir(iniDir);
    return 0;
}

static int ez_releasedir(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) do_moduleDir(rilPath);
    printf(">> %s directory released\n", rilPath);
    return 0;
}

static int ez_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *ingfoFile) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = 0, fd = 0;
    (void) ingfoFile;

    fd = open(rilPath, O_RDONLY);
    if(fd == -1) return -errno;

    hasil = pread(fd, buf, size, offset);
    if(hasil == -1) return -errno;

    close(fd);
    return hasil;
}

static int ez_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int inifile = open(rilPath, O_WRONLY);
    if(inifile == -1) return -errno;
    int hasil = pwrite(inifile, buf, size, offset);
    if(hasil == -1) return -errno;
    close(inifile);
    return hasil;
}

static int ez_open(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) fileNot_modular(rilPath);

    int hasil = open(rilPath, fi->flags);
    if(hasil == -1) return -errno;

    fi->fh = hasil;
    return 0;
}

static int ez_mkdir(const char *path, mode_t mode) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = mkdir(rilPath, mode);
    if(hasil == -1) return -errno;

    ez_logg(rilPath, "MKDIR", "REPORT");
    return 0;
}

static int ez_access(const char *path, int mask) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = access(rilPath, mask);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    (void) fi;

    int hasil = creat(rilPath, mode);
    if(hasil == -1) return -errno;

    close(hasil);
    ez_logg(rilPath, "CREATE", "REPORT");

    return 0;
}

static int ez_rename(const char *from, const char *to) {
    int hasil = 0;
    char rilFrom[256], rilTo[256];
    char laporkeun[1000];

    sprintf(rilFrom, "%s%s", dirPath, from);
    sprintf(rilTo, "%s%s", dirPath, to);

    if(!strstr(from, ".") && !strstr(to, ".")) {
        if(strstr(from, "module_") && !strstr(to, "module_")) {
            changeTo_not_modularDir(rilFrom);   
        } else if(!strstr(from, "module_") && strstr(to, "module_")) changeTo_modularDir(rilFrom);
    }
    if(strstr(from, ".") && strstr(to, ".")) {
        fileNot_modular(rilFrom);
    }

    hasil = rename(rilFrom, rilTo);
    if(hasil == -1) return -errno;

    if(strstr(from, ".") && strstr(to, ".")) {
        fileTo_modular(rilTo);
    }

    sprintf(laporkeun, "%s::%s", rilFrom, rilTo);
    ez_logg(laporkeun, "RENAME", "REPORT");

    return 0;
}

static int ez_unlink(const char *path) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = unlink(rilPath);
    if(hasil == -1) return -errno;

    ez_logg(rilPath, "UNLINK", "FLAG");
    return 0;
}

static int ez_rmdir(const char *path) {
    char rilPath[1000];
    int hasil;

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if((hasil = rmdir(rilPath)) == -1) return -errno;

    ez_logg(rilPath, "RMDIR", "FLAG");
    return 0;
}

static int ez_flush(const char *path, struct fuse_file_info *fi) {
    return 0;
}

static int ez_release(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) fileTo_modular(rilPath);
    return 0;
}

static int ez_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = lsetxattr(rilPath, name, value, size, flags);
    if(hasil == -1) return -errno;
    return 0;
}

static struct fuse_operations ez_opr = {
    .getattr = ez_getattr,
    .access = ez_access,
    .opendir = ez_opendir,
    .readdir = ez_readdir,
    .releasedir = ez_releasedir,
    .read = ez_read,
    .truncate = ez_truncate,
    .ftruncate = ez_ftruncate,
    .open = ez_open,
    .write = ez_write,
    .mkdir = ez_mkdir,
    .create = ez_create,
    .unlink = ez_unlink,
    .rmdir = ez_rmdir,
    .rename = ez_rename,
    .flush = ez_flush,
    .release = ez_release,
    .setxattr = ez_setxattr,
};

int main(int argc, char *argv[]) {
    umask(0);

    return fuse_main(argc, argv, &ez_opr, NULL);
}
```

## Penjelasan
Solusi dari soal ini sangatlah panjang, oleh karena itu kita hanya akan membahas bagian - bagian yang penting dari solusi ini.

Pada dasarnya, `easy.c` merupakan file c yang digunakan untuk menjalankan filesystem dengan library fuse secara umum namun dengan beberapa tambahan fungsi.

+ Fungsi `ez_logg`
```c
void ez_logg(const char *path, const char *aksi, const char *logtype) {
    char laporan[1000];
    char currtime[20];
    time_t wakt;
    struct tm *thisWaktu;

    time(&wakt);
    thisWaktu = localtime(&wakt);
    strftime(currtime, sizeof(currtime), "%y%m%d-%H:%M:%S", thisWaktu);
    sprintf(laporan, "%s::%s::%s::%s\n", logtype, currtime, aksi, path);

    FILE *inilog = fopen(logfile, "a");
    fprintf(inilog, "%s", laporan);
    printf("> Logged\n");

    fclose(inilog);
}
```
Fungsi ini digunakan untuk mencatat log aktivitas yang dilakukan pada filesystem sesuai dengan permintaan soal. Fungsi ini menerima input `path` yang merupakan alamat dari tempat dilakukannya command, `aksi` yang merupakan nama command yang akan dicatat, dan `logtype` yang merupakan jenis log yang akan dicatat (FLAG atau REPORT).

Saat berjalan, fungsi ini akan mencatat waktu dipanggilnya fungsi tersebut dan kemudian mencatatnya dalam file `fs_module.log` sesuai dengan format, Fungsi akan mencetak pesan "logged" ketika pencatatan berhasil dilakukan.

+ Fungsi `fileNot_modular`, `fileTo_modular`, `changeTo_not_modularDir`, `changeTo_modularDir`, `dont_moduleDir`, dan `do_moduleDir`
```c
struct file {
    char filename[64];
};


//only file
void fileNot_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char mergeFile[320];
        char rmFile[256];
        sprintf(mergeFile, "cat %s.* > %s", filePath, filePath);
        sprintf(rmFile, "rm %s.*", filePath);

        system(mergeFile);
        system(rmFile);
    } else return;
}


//only file
void fileTo_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char splitFile[320];
        char rmFile[256];
        sprintf(splitFile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filePath, filePath);
        sprintf(rmFile, "rm %s", filePath);

        system(splitFile);
        system(rmFile);
    } else return;
}


//change back recursive
void changeTo_not_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4 && (strcmp(de->d_name, ".")!=0 && strcmp(de->d_name, "..")!=0)) {
            char newPath[320];
            sprintf(newPath, "%s/%s", rilPath, de->d_name);
            changeTo_not_modularDir(newPath);
        }
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//change recursive
void changeTo_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) {
            if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
            else {
                char newPath[320];
                sprintf(newPath, "%s/%s", rilPath, de->d_name);
                changeTo_modularDir(newPath);
            }
        }
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}


//for releasedir
void dont_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//for readdir
void do_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) continue;
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}
```

Keenam fungsi ini memiliki kegunaan yang kurang lebih sama. Fungsi yang mengandung kata "Not" atau "dont" merupakan fungsi untuk mengubah file Modular menjadi tidak modular. Sementara yang tidak memiliki kata tersebut adalah fungsi untuk mengubah file biasa menjadi file modular. Fungsi yang memiliki kata "dir" merupakan fungsi yang akan mengubah seluruh file dalam direktori, sementara fungsi yang memiliki kata "file" adalah fungsi yang hanya akan mengubah file tersebut.

Setiap fungsi tersebut diletakkan dalam fungsi filesystem yang berbeda - beda
- fungsi `fileNot_modular` diletakkan pada fungsi `ez_open` sehingga setiap file yang dibuka pada filesystem akan diubah menjadi tidak modular terlebih dahulu dan dapat dibaca atau diedit.
- fungsi `fileTo_modular` diletakkan pada fungsi `ez_release` sehingga setiap file dalam direktori modular yang selesai dibaca atau diedit akan kembali ke posisi modular.
- fungsi `changeTo_not_modularDir` diletakkan pada fungsi `ez_rename` sehingga direktori modular dapat berubah menjadi direktori biasa setelah namanya diubah sehingga tidak mengandung kata "module_" di dalamnya.
- fungsi `changeTo_modularDir` diletakkan pada fungsi `ez_rename` dan `ez_create` sehingga direktori biasa modular dapat berubah menjadi direktori modular setelah namanya diubah sehingga mengandung kata "module_" di dalamnya serta direktori baru yang dibuat di dalam direktori modular akan ikut termodularisasi juga.
- fungsi `dont_moduleDir` diletakkan pada fungsi `ez_opendir` sehingga direktori modular dapat berubah menjadi direktori sementara saat direktori modular dibuka melalui filesystem.
- fungsi `do_moduleDir` diletakkan pada fungsi `ez_releasedir` sehingga direktori modular dapat berubah kembali menjadi direktori modular setelah dibuka melalui fungsi `ez_opendir` sebelumnya.
