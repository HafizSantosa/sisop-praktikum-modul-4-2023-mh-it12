#define FUSE_USE_VERSION 28
#define _XOPEN_SOURCE 500
#define HAVE_SETXATTR
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include<sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/xattr.h>

static const char *dirPath = "/home/tezla/moodular";
static const char *logfile = "/home/tezla/fs_module.log";

void ez_logg(const char *path, const char *aksi, const char *logtype) {
    char laporan[1000];
    char currtime[20];
    time_t wakt;
    struct tm *thisWaktu;

    time(&wakt);
    thisWaktu = localtime(&wakt);
    strftime(currtime, sizeof(currtime), "%y%m%d-%H:%M:%S", thisWaktu);
    sprintf(laporan, "%s::%s::%s::%s\n", logtype, currtime, aksi, path);

    FILE *inilog = fopen(logfile, "a");
    fprintf(inilog, "%s", laporan);
    printf("> Logged\n");

    fclose(inilog);
}

struct file {
    char filename[64];
};


//only file
void fileNot_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char mergeFile[320];
        char rmFile[256];
        sprintf(mergeFile, "cat %s.* > %s", filePath, filePath);
        sprintf(rmFile, "rm %s.*", filePath);

        system(mergeFile);
        system(rmFile);
    } else return;
}


//only file
void fileTo_modular(const char *filePath) {
    char savePath[128];
    strcpy(savePath, filePath);

    char *cek = strtok(savePath, ".");
    cek = strtok(NULL, ".");
    cek = strtok(NULL, ".");
    if(!cek) {
        char splitFile[320];
        char rmFile[256];
        sprintf(splitFile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filePath, filePath);
        sprintf(rmFile, "rm %s", filePath);

        system(splitFile);
        system(rmFile);
    } else return;
}


//change back recursive
void changeTo_not_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4 && (strcmp(de->d_name, ".")!=0 && strcmp(de->d_name, "..")!=0)) {
            char newPath[320];
            sprintf(newPath, "%s/%s", rilPath, de->d_name);
            changeTo_not_modularDir(newPath);
        }
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//change recursive
void changeTo_modularDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) {
            if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
            else {
                char newPath[320];
                sprintf(newPath, "%s/%s", rilPath, de->d_name);
                changeTo_modularDir(newPath);
            }
        }
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}


//for releasedir
void dont_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(strstr(de->d_name, ".001")) {
            strcpy(listFile[fileCount].filename, de->d_name);
            listFile[fileCount].filename[strlen(listFile[fileCount].filename) -4] = '\0';
            fileCount++;
        }
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i=0; i<fileCount; i++) {
        char filepath[128];
        char mergefile[512];
        char rmfile[256];

        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(mergefile, "cat %s.* > %s", filepath, filepath);
        sprintf(rmfile, "rm %s.*", filepath);

        system(mergefile);
        system(rmfile);
    }
}

//for readdir
void do_moduleDir(const char *rilPath) {
    struct file listFile[100];
    int fileCount = 0;
    DIR *toRead;
    struct dirent *de;

    toRead = opendir(rilPath);
    while((de = readdir(toRead)) != NULL) {
        if(de->d_type == 4) continue;
        strcpy(listFile[fileCount].filename, de->d_name);
        fileCount++;
    }
    closedir(toRead);
    if(fileCount == 0) return;

    for(int i = 0; i<fileCount; i++) {
        char filepath[128];
        char splitfile[512];
        char rmfile[256];
        sprintf(filepath, "%s/%s", rilPath, listFile[i].filename);
        sprintf(splitfile, "split -b1K -a3 -d --numeric-suffixes=1 %s %s.", filepath, filepath);
        sprintf(rmfile, "rm %s", filepath);

        system(splitfile);
        system(rmfile);
    }
}

static int ez_getattr(const char *path, struct stat *statbuf) {
    int hasil;
    char rilPath[256];
    sprintf(rilPath, "%s%s", dirPath, path);

    hasil = lstat(rilPath, statbuf);
    if(hasil == -1) return -errno;

    return 0;
}

static int ez_truncate(const char *path, off_t size) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil;
    hasil = truncate(rilPath, size);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil;
    hasil = ftruncate(fi->fh, size);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_opendir(const char *path, struct fuse_file_info *fi) {
    (void)fi;
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) dont_moduleDir(rilPath);
    return 0;
}

static int ez_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *ingfoFile) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    DIR *iniDir;
    struct dirent *de;
    (void) offset;
    (void) ingfoFile;

    int hasil = 0;

    iniDir = opendir(rilPath);
    if(iniDir == NULL) return -errno;

    while((de = readdir(iniDir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type <<12;

        hasil = (filler(buf, de->d_name, &st, 0));

        if(hasil != 0) break;
    }

    closedir(iniDir);
    return 0;
}

static int ez_releasedir(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) do_moduleDir(rilPath);
    printf(">> %s directory released\n", rilPath);
    return 0;
}

static int ez_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *ingfoFile) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = 0, fd = 0;
    (void) ingfoFile;

    fd = open(rilPath, O_RDONLY);
    if(fd == -1) return -errno;

    hasil = pread(fd, buf, size, offset);
    if(hasil == -1) return -errno;

    close(fd);
    return hasil;
}

static int ez_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int inifile = open(rilPath, O_WRONLY);
    if(inifile == -1) return -errno;
    int hasil = pwrite(inifile, buf, size, offset);
    if(hasil == -1) return -errno;
    close(inifile);
    return hasil;
}

static int ez_open(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) fileNot_modular(rilPath);

    int hasil = open(rilPath, fi->flags);
    if(hasil == -1) return -errno;

    fi->fh = hasil;
    return 0;
}

static int ez_mkdir(const char *path, mode_t mode) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = mkdir(rilPath, mode);
    if(hasil == -1) return -errno;

    ez_logg(rilPath, "MKDIR", "REPORT");
    return 0;
}

static int ez_access(const char *path, int mask) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = access(rilPath, mask);
    if(hasil == -1) return -errno;
    return 0;
}

static int ez_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    (void) fi;

    int hasil = creat(rilPath, mode);
    if(hasil == -1) return -errno;

    close(hasil);
    ez_logg(rilPath, "CREATE", "REPORT");

    return 0;
}

static int ez_rename(const char *from, const char *to) {
    int hasil = 0;
    char rilFrom[256], rilTo[256];
    char laporkeun[1000];

    sprintf(rilFrom, "%s%s", dirPath, from);
    sprintf(rilTo, "%s%s", dirPath, to);

    if(!strstr(from, ".") && !strstr(to, ".")) {
        if(strstr(from, "module_") && !strstr(to, "module_")) {
            changeTo_not_modularDir(rilFrom);   
        } else if(!strstr(from, "module_") && strstr(to, "module_")) changeTo_modularDir(rilFrom);
    }
    if(strstr(from, ".") && strstr(to, ".")) {
        fileNot_modular(rilFrom);
    }

    hasil = rename(rilFrom, rilTo);
    if(hasil == -1) return -errno;

    if(strstr(from, ".") && strstr(to, ".")) {
        fileTo_modular(rilTo);
    }

    sprintf(laporkeun, "%s::%s", rilFrom, rilTo);
    ez_logg(laporkeun, "RENAME", "REPORT");

    return 0;
}

static int ez_unlink(const char *path) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = unlink(rilPath);
    if(hasil == -1) return -errno;

    ez_logg(rilPath, "UNLINK", "FLAG");
    return 0;
}

static int ez_rmdir(const char *path) {
    char rilPath[1000];
    int hasil;

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if((hasil = rmdir(rilPath)) == -1) return -errno;

    ez_logg(rilPath, "RMDIR", "FLAG");
    return 0;
}

static int ez_flush(const char *path, struct fuse_file_info *fi) {
    return 0;
}

static int ez_release(const char *path, struct fuse_file_info *fi) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    if(strstr(rilPath, "module_")) fileTo_modular(rilPath);
    return 0;
}

static int ez_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char rilPath[1000];

    if(strcmp(path, "/") == 0) {
        path = dirPath;
        sprintf(rilPath, "%s", path);
    } else sprintf(rilPath, "%s%s", dirPath, path);

    int hasil = lsetxattr(rilPath, name, value, size, flags);
    if(hasil == -1) return -errno;
    return 0;
}

static struct fuse_operations ez_opr = {
    .getattr = ez_getattr,
    .access = ez_access,
    .opendir = ez_opendir,
    .readdir = ez_readdir,
    .releasedir = ez_releasedir,
    .read = ez_read,
    .truncate = ez_truncate,
    .ftruncate = ez_ftruncate,
    .open = ez_open,
    .write = ez_write,
    .mkdir = ez_mkdir,
    .create = ez_create,
    .unlink = ez_unlink,
    .rmdir = ez_rmdir,
    .rename = ez_rename,
    .flush = ez_flush,
    .release = ez_release,
    .setxattr = ez_setxattr,
};

int main(int argc, char *argv[]) {
    umask(0);

    return fuse_main(argc, argv, &ez_opr, NULL);
}